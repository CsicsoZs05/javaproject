/**
 * Created by Csicsószki Zsombor on 3/14/2017.
 */
public class GuessGame extends Player {

  private Player p1;
  private Player p2;
  private Player p3;

  public Player getP1() {
    return p1;
  }

  public void setP1(Player p1) {
    this.p1 = p1;
  }

  public Player getP2() {
    return p2;
  }

  public void setP2(Player p2) {
    this.p2 = p2;
  }

  public Player getP3() {
    return p3;
  }

  public void setP3(Player p3) {
    this.p3 = p3;
  }

  public void startGame() {

    p1 = new Player();
    p2 = new Player();
    p3 = new Player();

    int guessp1 = 0;
    int guessp2 = 0;
    int guessp3 = 0;

    boolean p1isright = false;
    boolean p2isright = false;
    boolean p3isright = false;

    int targetNumber = (int) (Math.random() * 10);
    System.out.println("I'm thinking of a number between 0 and 9...");

    do {
      System.out.println("Number to guess is " + targetNumber);

      p1.guess();
      p2.guess();
      p3.guess();

      guessp1 = p1.getNumber();
      System.out.println("Player1 guessed " + guessp1);

      guessp2 = p2.getNumber();
      System.out.println("Player2 guessed " + guessp2);

      guessp3 = p3.getNumber();
      System.out.println("Player3 guessed " + guessp3);

      if (guessp1 == targetNumber) {
        p1isright = true;
      }

      if (guessp2 == targetNumber) {
        p2isright = true;
      }

      if (guessp3 == targetNumber) {
        p3isright = true;
      }

      if (p1isright || p2isright || p3isright) {

        System.out.println("Winner");
        System.out.println("P1 got it right?" + p1isright);
        System.out.println("P2 got it right?" + p2isright);
        System.out.println("P3 got it right?" + p3isright);
        System.out.println("Game Over");

      } else {

        System.out.println("Try again");
      }

    } while (p1isright || p2isright || p3isright);


  }

}
