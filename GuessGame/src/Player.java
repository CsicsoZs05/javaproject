/**
 * Created by Csicsószki Zsombor on 3/14/2017.
 */
public class Player {

  private int number = 0;

  public int getNumber() {
    return number;
  }

  public void setNumber(int number) {
    this.number = number;
  }

  public void guess() {
    number = (int) (Math.random() * 10);
    System.out.println("I'm guessing " + number);

  }
}
