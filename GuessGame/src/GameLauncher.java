/**
 * Created by Csicsószki Zsombor on 3/14/2017.
 */
public class GameLauncher {

  public static void main(String[] args) {

    GuessGame game = new GuessGame();
    game.startGame();

  }

}
